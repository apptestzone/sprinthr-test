<?php
class G_Validate_Permission {

	protected $module;
	protected $parent_index;
	protected $child_index;
	protected $show_error_page;

	protected $user_role;

	public function __construct($data) {
		$this->user_role = $data;
	}

	public function setModule($value) {
		$this->module = $value;
	}

	public function setParentIndex($value) {
		$this->parent_index = $value;
	}

	public function setChildIndex($value) {
		$this->child_index = $value;
	}

	public function setShowErrorPage($value) {
		$this->show_error_page = $value;
	}

	public function getUserPermission() {

		$m = new G_Sprint_Modules($this->module);
		$actions = $m->getModuleActions($this->parent_index, $this->child_index);

		//get user role 
		$user_roles = $this->user_role;
		if(!is_array($user_roles) && !empty($user_roles)) {
			$user_roles = trim($user_roles);
			if( $user_roles == G_Employee_User::OVERRIDE_HR_ACCESS || $user_roles == G_Employee_User::OVERRIDE_PAYROLL_ACCESS || $user_roles == G_Employee_User::OVERRIDE_DTR_ACCESS || $user_roles == G_Employee_User::OVERRIDE_EMPLOYEE_ACCESS ){    				
				return true;
			}
		}

		if(!empty($this->child_index)) {
			$index = $this->child_index;
		}else{
			$index = $this->parent_index;
		}
		
		foreach($user_roles as $key => $value) {
			//SUPER USER
			$module_name = trim($value['module']);  
			if( $module_name == G_Employee_User::OVERRIDE_HR_ACCESS || $module_name == G_Employee_User::OVERRIDE_PAYROLL_ACCESS || $module_name == G_Employee_User::OVERRIDE_DTR_ACCESS || $module_name == G_Employee_User::OVERRIDE_EMPLOYEE_ACCESS){    				
				return true;
			}

			$return = self::checkPermission($value['module'], $index, $value['action'], $actions);
			if($return['is_success']) {
				break;
			}

		}		
		if(!$return['is_success']) {
			if($this->show_error_page) {
				include APP_PATH . 'errors/credentials_required.php';
				die();
			}
		}

		return $return['action'];		
	}

	public function checkPermission($module = '', $index = '', $action = '', $actions = array()) {
		$return['is_success'] 	= false;
		$return['action']		= '';
		if(trim($module) == trim($index)) {
			if(in_array(trim($action), $actions)) {
				if(trim($action) == Sprint_Modules::PERMISSION_04) {

				}else{
					$return['is_success'] 	= true;
					$return['action']		= $action;
					return $return;
					/*$return = true;
					$action = $value['action'];*/
				}
			}
		} 
		return $return;
	}
	
}
?>