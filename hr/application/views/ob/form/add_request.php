<style>
.leave-header{padding:4px;background-color: #198cc9;color:#ffffff;margin-top:9px;line-height: 27px;}
</style>
<script>
var date_from_str = $("#date_from").val();
var date_to_str   = $("#date_to").val();

$(document).ready(function() {		
	$('#add_request_form').validationEngine({scroll:false});	
		
	$('#add_request_form').ajaxForm({
		success:function(o) {
			if (o.is_success == 1) {
				load_ob_list_dt(o.from,o.to);		
				hide_add_ob_form();
				closeDialog('#' + DIALOG_CONTENT_HANDLER);	
				dialogOkBox(o.message,{});						
			} else {
				hide_add_ob_form();
				closeDialog('#' + DIALOG_CONTENT_HANDLER);	
				dialogOkBox(o.message,{});			
			}
		},
		dataType:'json',
		beforeSubmit: function() {
			showLoadingDialog('Saving...');
		}
	});	
	
	$("#ob_date_from").datepicker({
		minDate: date_from_str,
    	maxDate: date_to_str,
		dateFormat:'yy-mm-dd',
		changeMonth:true,
		changeYear:true,
		showOtherMonths:true,
		onSelect	:function() { 
			$("#ob_date_to").datepicker('option',{minDate:$(this).datepicker('getDate')});
		}
	});	
	
	$("#ob_date_to").datepicker({
		minDate: date_from_str,
    	maxDate: date_to_str,
		dateFormat:'yy-mm-dd',
		changeMonth:true,
		changeYear:true,
		showOtherMonths:true,
		onSelect	:function() { 
		
		}
	});	
	
	var t = new $.TextboxList('#employee_id', {
		unique: true,
		max:1,
		plugins: {
			autocomplete: {
				minLength: 2,				
				onlyFromValues: true,
				queryRemote: true,
				remote: {url: base_url + 'ob/ajax_get_employees_autocomplete'}			
			}
	}});

	t.addEvent('blur',function(o) {		
		load_show_employee_request_approvers();
	});
	
});
</script>
<div id="formcontainer">
<form id="add_request_form" name="add_request_form" action="<?php echo url('ob/_save_ob_request'); ?>" method="post"> 
<input type="hidden" id="token" name="token" value="<?php echo $token; ?>" />
<input type="hidden" id="date_from" name="date_from" value="<?php echo $from; ?>" />
<input type="hidden" id="date_to" name="date_to" value="<?php echo $to; ?>" />
<div id="formwrap">	
	<h3 class="form_sectiontitle">Add New Request</h3>
<div id="form_main">     
  
    <div id="form_default">      
        <table>        	 
             <tr>
               <td class="field_label">Type Employee Name:</td>
               <td>
               		<input class="validate[required] input-large" type="text" name="employee_id" id="employee_id" value="" />
               </td>
             </tr>
        </table>
        <div id="show_request_approvers_wrapper"></div>
        <h3 class="leave-header">Official Business Details</h3> 
        <table>
             <tr>
               <td class="field_label">From:</td>
               <td>
               		<input class="validate[required] input-small" type="text" name="ob_date_from" id="ob_date_from" value="" />
               </td>
             </tr>  
             <tr>
               <td class="field_label">To:</td>
               <td>
               		<input class="validate[required] input-small" type="text" name="ob_date_to" id="ob_date_to" value="" />
               </td>
             </tr>                                                                   
             <!-- <tr>
               <td class="field_label">Is Approved:</td>
               <td>
               		<select class="validate[required] select_option" name="is_approved" id="is_approved">        
               		<option value="<?php //echo Employee_Official_Business_Request::YES; ?>" selected="selected"><?php //echo Employee_Official_Business_Request::YES; ?></option>  
                    <option value="<?php //echo Employee_Official_Business_Request::NO; ?>"><?php //echo Employee_Official_Business_Request::NO; ?></option>                                  
                    </select>
               </td>
             </tr> -->
             <tr>
               <td class="field_label">Comments:</td>
               <td>
               		<textarea class="input-large" rows="3" id="comments" name="comments"></textarea>               		
               </td>
             </tr>                                  
         </table>
    </div>
    <div id="form_default" class="form_action_section">
    	<table width="100%" border="0" cellpadding="0" cellspacing="0">
        	<tr>
            	<td class="field_label">&nbsp;</td>
                <td>
                <input type="submit" value="Save" class="curve blue_button" />
                <a href="javascript:void(0)" onclick="javascript:hide_add_ob_form();">Cancel</a>
                </td>
            </tr>
        </table>
    </div>
</div><!-- #form_main -->
</div>
</form>
</div>

