<table width="52%" bgcolor="#ffffff" border="0" align="center" cellpadding="6" cellspacing="1">
<thead>
<tr>    
    <td width="40%" bgcolor="#CCCCCC">Employee Name</td>
    <td width="20%" bgcolor="#CCCCCC" style="text-align:center;">Type (In or Out)</td>
    <td width="25%" bgcolor="#CCCCCC">Time and Date</td>
  </tr>
</thead>
<tbody>
<?php 
$counter = 1;
foreach ($records as $r):?>
<?php
$date = $r->getDate();
if ($date == date('Y-m-d', strtotime('today'))) {
  $date_string = 'Today'; 
} else {
  $date_string = Tools::convertDateFormat($date); 
}
?>
<?php if ($counter == 1):?>
  <tr>    
    <td><?php echo $r->getEmployeeName();?></td>
    <td style="text-align:center;"><?php echo strtoupper($r->getType());?></td>
    <td><?php echo date('g:i a', strtotime($r->getTime()));?> - <?php echo $date_string;?></td>
  </tr>
<?php else:?>
  <tr>   
    <td><?php echo $r->getEmployeeName();?></td>
    <td style="text-align:center;"><?php echo strtoupper($r->getType());?></td>
    <td><?php echo date('g:i a', strtotime($r->getTime()));?> - <?php echo $date_string;?></td>
  </tr>
<?php endif;?>
<?php $counter++;?> 
<?php
$date_string = '';
$date = '';
?>
<?php endforeach;?>
</tbody>
</table>