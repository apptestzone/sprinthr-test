<br /><br />
<table border="1" cellpadding="2" cellspacing="1" style="font-size:9pt; width:836pt; line-height:12pt;">
	<tr>
        <td align="center" valign="middle" style="width:90pt; vertical-align:middle;"><strong>Employee Code</strong></td>
        <td align="center" valign="top" style="border-bottom:none;"><strong>Employee Name</span></strong></td>            
        <td align="center" valign="top" style="border-bottom:none;"><strong>Department Name</span></strong></td>            
        <td align="center" valign="top" style="border-bottom:none;"><strong>Section Name</span></strong></td>            
        <td align="center" valign="top" style="border-bottom:none;"><strong>Position</span></strong></td>            
        <td align="center" valign="top" style="border-bottom:none;"><strong>Date</span></strong></td>            
        <td align="center" valign="top" style="border-bottom:none;"><strong>Time In</span></strong></td>            
        <td align="center" valign="top" style="border-bottom:none;"><strong>Time Out</span></strong></td>     
        <td align="center" valign="top" style="border-bottom:none;"><strong>Total Hours</span></strong></td>     
        <td align="center" valign="top" style="border-bottom:none;"><strong>Remarks</span></strong></td>          
    </tr>
	<?php foreach($daily_time_record as $a){ ?>
        <?php
            //echo '<pre>';
            //print_r($a);
            //echo '</pre>';
        ?>
    	<tr>
            <?php 
            $employee_name = strtr(utf8_decode($a['employee_name']), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
            ?>
            <td align="left" valign="top" style="border-bottom:none;"><?php echo $a['employee_code']; ?></td>
            <td align="left" valign="top" style="border-bottom:none;"><?php echo mb_convert_case($employee_name, MB_CASE_TITLE, "UTF-8"); ?></td>
            <td align="left" valign="top" style="border-bottom:none;"><?php echo mb_convert_case($a['department_name'], MB_CASE_TITLE, "UTF-8"); ?></td>
            <td align="left" valign="top" style="border-bottom:none;"><?php echo mb_convert_case($a['section'], MB_CASE_TITLE, "UTF-8"); ?></td>
            <td align="left" valign="top" style="border-bottom:none;"><?php echo mb_convert_case($a['position'], MB_CASE_TITLE, "UTF-8"); ?></td>
            <td align="left" valign="top" style="border-bottom:none;"><?php echo $a['date_attendance']; ?></td>
            <td align="left" valign="top" style="border-bottom:none;"><?php echo $a['actual_time_in']; ?></td>
            <td align="left" valign="top" style="border-bottom:none;"><?php echo $a['actual_time_out']; ?></td>
            <td align="left" valign="top" style="border-bottom:none;"><?php echo number_format(Tools::computeHoursDifferenceByDateTime($a['actual_time_in'], $a['actual_time_out']),2); ?></td>
            <td align="left" valign="top" style="border-bottom:none;">
                <?php 
                    $remark = "";
                    if($a['is_restday'] == 1) { //restday
                        $remark = "Restday";
                    }elseif($a['is_restday'] == 1 && empty($a['actual_time_in']) && empty($a['actual_time_out'])) {
                        $remark = "Restday";
                    }elseif($a['is_holiday'] == 1 && $a['holiday_type'] == 1) { //holiday legal
                        $remark = "Holiday Legal";
                    }elseif($a['is_holiday'] == 1 && $a['holiday_type'] == 2) { //holiday special
                        $remark = "Holiday Special";
                    }elseif($a['is_holiday'] == 1){
                        $remark = "Holiday";
                    }elseif($a['is_ob'] == 1) {
                        $remark = "Ob";
                    }elseif($a['is_leave'] == 1 && $a['leave_id'] != 0) {
                        $leave = G_Leave_Finder::findById($a['leave_id']);
                        $remark = $leave->getName();
                    }elseif($a['is_leave'] == 1 && $a['leave_id'] == 0) {
                        $remark = "Leave";
                    }elseif($a['actual_time_in'] == '' && !empty($a['actual_time_out'])) { //no time in
                        $remark = "No In";
                    }elseif($a['actual_time_out'] == '' && !empty($a['actual_time_in'])) { //no timeout
                        $remark = "Out";
                    }elseif($a['is_present'] == 1 && $a['actual_time_in'] == '' && !empty($a['actual_time_out'])) { //no time in
                        $remark = "No In";
                    }elseif($a['is_present'] == 1 && $a['actual_time_out'] == '' && !empty($a['actual_time_in'])) { //no timeout
                        $remark = "Out";
                    }elseif($a['is_present'] == 0 && empty($a['actual_time_in']) && empty($a['actual_time_out'])) { //absent
                        $remark = "Absent";
                    }elseif($a['is_present'] == 0 && $a['is_paid'] == 0 && $a['actual_time_in'] != '' && $a['actual_time_out']  != ''){
                        $remark = "Incorrect Shift";
                    }
                    echo $remark;
                ?>  
            </td>
        </tr>
    <?php } ?>   
</table>	


